# Handbook Helper

A Slack bot that sends friendly reminders to update the [GitLab Handbook](https://about.gitlab.com/handbook/) when questions are answered in Slack